# Что делать, если веб-ресурсы KDE недоступны

KDE — международное сообщество, разрабатывающее свободное программное обеспечение: среду рабочего стола Plasma и большой набор приложений. KDE не компания, коммерческой деятельности в России не ведёт и под санкционные ограничения не попадает. Тем не менее последние события, в том числе блокировка доступа ко многим сайтам, заставляют задуматься о ситуации, когда ресурсы KDE в Интернете могут оказаться недостижимы для пользователей на территории РФ. Напомним, в 2018 году, по причине блокировки Роскомнадзором мессенджера Telegram, доступ к серверам KDE уже [нарушался](https://vk.com/wall-33239_1761).

Мы ценим достижения разработчиков, переводчиков и других участников многолетнего труда над продуктами KDE, поэтому хотим, чтобы они были доступны всем и каждому несмотря ни на что. Более того, мы надеемся, что ограничения импорта проприетарных программ побудят россиян к повсеместному внедрению свободных информационных технологий, а не пиратству.

## Сборки программного обеспечения

Большинство сборок ПО KDE, попадающих в руки пользователей, подготавливаются и распространяются другими проектами: дистрибутивами GNU/Linux. Соответственно, блокировка ресурсов KDE не лишит конечных потребителей доступа к программам. Непосредственно в России сборки программного обеспечения KDE распространяют:

* компания Базальт СПО с дистрибутивами [Альт Рабочая станция К](https://www.basealt.ru/alt-workstation/description) и [Альт Образование](https://www.basealt.ru/alt-education/description);
* компания НТЦ ИТ РОСА c дистрибутивами [ROSA Fresh](https://www.rosalinux.ru/rosa-fresh-12/) и [РОСА «ХРОМ» Рабочая станция](https://www.rosalinux.ru/enterprise-chrome/).

![Дистрибутив](https://kde.ru/img/distros/bg/alt.png)

Самостоятельно KDE подготавливает сборки приложений для ОС Windows, macOS и Android. Некоторые из них публикуются, в том числе, на сервере [download.kde.org](https://download.kde.org), данные которого в РФ дублируются [зеркалом Яндекса](https://mirror.yandex.ru/mirrors/ftp.kde.org/stable/).

![Зеркало](mirror.png)

Другие, к сожалению, вне магазинов приложений доступны только на [KDE Binary Factory](https://binary-factory.kde.org) — обдумайте создание резервных копий установщиков нужных вам программ.

![Binary Factory](binary-factory.png)

## Исходный код

Программный код — это знания, из которых состоит наше программное обеспечение, результат десятилетий работы волонтёров со всего мира, наш ценнейший ресурс. Помимо собственного GitLab-сервера KDE, [Invent](https://invent.kde.org), исходный код можно получить с [зеркала на GitHub](https://github.com/KDE).

![Invent](invent.png)

Но что делать, если оба этих сайта окажутся недоступны?

### Архив

Мы не знаем, какие препятствия ждут российский Интернет, поэтому пошли дальше и опубликовали в Архиве Интернета полную резервную копию Git-репозиториев KDE:

🗄️ [archive.org/details/kde-git-repositories](https://archive.org/details/kde-git-repositories)

![Кнопка загрузки архива](download-button.png)

Это архив размером около 9 Гб, где репозитории представлены в виде файлов Git Bundle. Для их распаковки можно использовать команду:

```sh
git clone -b master ИМЯ.bundle
```

Обратите внимание на возможность загрузки через торрент-клиент.

### Самостоятельное клонирование

Другой вариант резервного копирования — самим склонировать значимые Git-репозитории. Для этого потребуются Git и Python 3:

```sh
mkdir kde-src && cd kde-src
git clone https://invent.kde.org/sysadmin/repo-metadata
rm -rf ./repo-metadata/projects-invent/{historical,unmaintained}
./repo-metadata/git-helpers/git-kclone "*"
```

Без сжатия репозитории суммарно займут около 30 Гб дискового пространства.

## Разработка

Мы поговорили о настоящем и прошлом, но совсем не затронули будущее. Как разработчикам из РФ продолжать участие в проекте, если недоступным окажется [Invent](https://invent.kde.org)?

Увы, почти никак. Если доступ к нему сохранится у вашего поставщика услуг электронной почты, вы можете попробовать [отправку патчей по e-mail](https://docs.gitlab.com/14.8/ee/user/project/merge_requests/creating_merge_requests.html#by-sending-an-email). Для этого стоит заранее сохранить на компьютере специальные секретные адреса, отправка на которые файлов `.patch` создаёт запросы на слияние (MR) от вашего имени. Разумеется, без доступа к актуальным версиям исходного кода активно развивающихся проектов это будет работать недолго — но, может быть, кому-то этот способ пригодится.

![Запрос на слияние по электронной почте](mr-email.png)

## Информационные ресурсы

Сайт [KDE.ru](https://kde.ru) физически размещён в Германии, на одном сервере с глобальным сайтом, [KDE.org](https://kde.org). В то же время сервер списка рассылки переводчиков на русский, [lists.kde.ru](https://lists.kde.ru), находится в РФ.

В случае масштабных блокировок иностранных Интернет-ресурсов наиболее доступным источником информации будет [наша группа ВКонтакте](https://vk.com/kde_ru).

## Как вы можете помочь

Если вы представляете компанию или образовательное учреждение, деятельность которых связана с информационными технологиями в России, либо просто обладаете достаточными ресурсами, рассмотрите возможность [создания собственного зеркала KDE](https://kde.org/mirrors/ftp_howto/).

Если вы скачали нашу [резервную копию исходного кода](https://archive.org/details/kde-git-repositories) из Архива Интернета, оставьте её на раздаче в вашем торрент-клиенте — делиться свободным ПО у нас пока полностью законно 😉

Наконец, сохраните и распространите эту инструкцию. Мы постараемся по мере необходимости обновлять её.

*До связи!* ✌️

![Баннер](cheburnet.png)
