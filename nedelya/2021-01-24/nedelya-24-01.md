# На этой неделе в KDE: бета-версия Plasma 5.21!

> 17–24 января, основное — прим. переводчика

Ну что, друзья, у вас наконец-то появился шанс опробовать [Plasma 5.21 в форме бета-выпуска](https://kde.org/announcements/plasma/5/20.90/)! Пожалуйста, [установите её](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) и поищите все ошибки, которые мы упустили. Отчёты об ошибках уже начали литься рекой, и мы исправим их как можно быстрее в следующем месяце. 🙂

А пока посмотрите, что ещё мы сделали на этой неделе:

## Новые возможности

У Kate [появилась палитра команд в стиле HUD с возможностью поиска, позволяющая вам активировать элементы меню с невероятной скоростью](https://invent.kde.org/utilities/kate/-/merge_requests/179)! Она активируется сочетанием клавиш Ctrl+Alt+I, и мы изучаем возможность её добавления и в другие приложения KDE в виде переиспользуемого библиотечного компонента (Waqar Ahmed, Kate 21.04):

![0](https://pointieststick.files.wordpress.com/2021/01/screenshot_20210119_075001.jpeg?w=1024)

## Исправления

Dolphin [теперь корректно отображает число файлов на дисках, не использующих стандартные файловые системы Linux (например, с NTFS)](https://bugs.kde.org/show_bug.cgi?id=430778) (Илья Кац, Dolphin 20.12.2)

Действие «Добавить сетевую папку» [снова видно в Dolphin для пользователей Frameworks 5.78 и более новых версий, хотя теперь оно живёт в панели инструментов, а не в представлении](https://bugs.kde.org/show_bug.cgi?id=431626) (Norbert Preining, Dolphin 20.12.2)

Страница «Шрифты» Параметров системы [больше не отображает пустой предпросмотр для вариантов сглаживания при использовании проприетарного драйвера Nvidia](https://bugs.kde.org/show_bug.cgi?id=408606) (Ярослав Сидловский, Plasma 5.21)

Discover [теперь обрабатывает ситуацию, когда вы устанавливаете дополнение, требующее выбрать вариант файла из списка](https://bugs.kde.org/show_bug.cgi?id=430812), как обычный диалог «Загрузить \[что-то\]» (Dan Leinir Turthra Jensen, Plasma 5.21)

Поддержка Pulse Connect Secure VPN в Plasma [теперь работает](https://invent.kde.org/plasma/plasma-nm/-/merge_requests/44) (David Redondo, Plasma 5.21)

Теперь возможно [убрать глобальные комбинации клавиш, установленные для активации виджетов Plasma](https://bugs.kde.org/show_bug.cgi?id=429500) (David Redondo, Plasma 5.21)

В сеансе Plasma Wayland [окна больше не пытаются привязаться к OSD и уведомлениям](https://invent.kde.org/plasma/kwin/-/merge_requests/598) (Влад Загородний, Plasma 5.21)

Имена файлов в диалогах выбора файлов GTK с темой Breeze [теперь читаемы, особенно при использовании тёмной цветовой схемы](https://bugs.kde.org/show_bug.cgi?id=431839) (Jan Blackquill, Plasma 5.21)

Отмена удаления файла или папки [больше не вызывает неожиданную перезапись существующего элемента, у которого такое же имя, как и у восстанавливаемого файла](https://invent.kde.org/frameworks/kio/-/merge_requests/319) (David Faure, Frameworks 5.79)

Параметры системы [больше не падают при переходе в раздел «Внешний вид» на странице блокировки экрана и выходе оттуда](https://invent.kde.org/frameworks/kirigami/-/merge_requests/210) (Nicolas Fella, Frameworks 5.79)

Okular (и, потенциально, другие приложения KDE) [снова могут открывать файлы, доступные из веб-браузера (напр. https://www.example.ru/my\_awesome\_file.pdf), используя диалог «Открыть»](https://bugs.kde.org/show_bug.cgi?id=431454) (Albert Astals Cid, Frameworks 5.79)

Виджеты системного лотка, которые предоставляют расширяемый список действий, [больше не обрезают иногда часть действий при использовании нестандартных семейств или размеров шрифтов](https://invent.kde.org/frameworks/plasma-framework/-/commit/8b16de4f6b54e3762d96853dcee94f811b08708b) (Nate Graham, Frameworks 5.79)

## Улучшения производительности

Discover [теперь запускается чуть быстрее и использует меньше памяти](https://bugs.kde.org/show_bug.cgi?id=431364) (всё ещё много, но хотя бы не столько) (Aleix Pol Gonzalez, Plasma 5.21)

## Улучшения пользовательского интерфейса

Действие Dolphin «Копировать расположение» [сменило сочетание клавиш на Ctrl+Alt+C](https://bugs.kde.org/show_bug.cgi?id=426461), так что оно не конфликтует с действием «Копировать» во встроенной панели терминала, где сочетание клавиш Ctrl+Shift+C («The Imp», Dolphin 20.12.2)

Панель заголовка Gwenview [теперь показывает путь просматриваемого расположения в режиме обзора](https://bugs.kde.org/show_bug.cgi?id=393314) (Antonio Prcela, Gwenview 21.04)

При использовании общесистемного режима открытия двойным щелчком [вы можете переименовывать файлы на рабочем столе щелчком по имени уже выбранного элемента, как и в Dolphin](https://bugs.kde.org/show_bug.cgi?id=392731) (Nate Graham, Plasma 5.21)

Повёрнутые виджеты [больше не имеют неровностей и сглажены](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/583) (David Edmundson, Plasma 5.21):

![1](https://pointieststick.files.wordpress.com/2021/01/pretty-rotated-widget.jpeg?w=1024)

Панель инструментов режима редактирования [теперь включает ссылку на страницу «Оформление рабочей среды» Параметров системы](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/311#note_171259) («Wael CH», Plasma 5.21):

![2](https://pointieststick.files.wordpress.com/2021/01/global-themes-button.jpeg?w=1024)

Страницы [«Эффекты»](https://invent.kde.org/plasma/kwin/-/merge_requests/588) и [«Управление службами»](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/299) Параметров системы теперь поддерживают функцию «Выделить изменённые параметры» (Cyril Rossi, Plasma 5.21)

Значки, используемые для поворота экрана на странице «Настройка экранов» Параметров системы, [стали понятнее](https://invent.kde.org/plasma/kscreen/-/merge_requests/8) («Wael CH», Plasma 5.21):

![3](https://pointieststick.files.wordpress.com/2021/01/move-obvious-orientation-icons.jpeg?w=1024)

Монохромные значки темы Breeze [теперь остаются монохромными при использовании масштабирования больше 200%](https://bugs.kde.org/show_bug.cgi?id=431475) (Kai Uwe Broulik, Frameworks 5.79)

## Как вы можете помочь

Прочитайте эту [статью, чтобы узнать](https://kde.ru/join), как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку фонду](https://kde.org/community/donations/) [KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [boingo-00](https://t.me/boingo00)  
*Источник:* <https://pointieststick.com/2021/01/23/this-week-in-kde-the-plasma-5-20-beta-is-here/>  
