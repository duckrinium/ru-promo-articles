# На этой неделе в KDE: офлайн-обновления можно отключить

> 11–18 апреля, основное — прим. переводчика

У нас хорошие новости для тех, кому не понравился переход KDE Neon на офлайн-обновления по умолчанию: теперь это можно настроить с помощью графического интерфейса (раньше уже была возможность сделать это с помощью интерфейса командной строки). Несмотря на то, что офлайн-обновления могут надоедать, они действительно помогают улучшить стабильность системы. Мы получаем сотни отчётов об ошибках, связанных с проблемами, которые вызваны исключительно тем, что компьютер не был перезагружен после обновления. Однако мы хотим, чтобы решение оставалось за вами, так что, [начиная с Plasma 5.22, офлайн-обновления можно включать и отключать в Параметрах системы](https://invent.kde.org/plasma/discover/-/merge_requests/111):

![0](https://pointieststick.files.wordpress.com/2021/04/screenshot_20210414_210403.png?w=1024)

## Новые возможности

Виджет глобального меню в сеансе Plasma Wayland [получил поисковую строку](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/134), которая поможет быстрее найти нужный элемент меню! (Jan Blackquill, Plasma 5.22)

Трансляция экрана в сеансе Wayland теперь по умолчанию [будет запускать режим «Не беспокоить»](https://invent.kde.org/plasma/xdg-desktop-portal-kde/-/merge_requests/33) ([это настраивается](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/425)) (Kai Uwe Broulik, Plasma 5.22)

Центр приложений Discover [теперь умеет выполнять обновления дистрибутивов](https://invent.kde.org/plasma/discover/-/merge_requests/109), использующих [rpm-ostree](https://coreos.github.io/rpm-ostree/), таких как Fedora Silverblue и [Fedora Kinoite](https://fedoramagazine.org/discover-fedora-kinoite/) (Mariam Fahmy, Plasma 5.22)

## Wayland

Теперь в сеансе Wayland [можно изменить значения вылетов развёртки](https://invent.kde.org/plasma/libkscreen/-/merge_requests/21) (Xaver Hugl, Plasma 5.22)

[Запуск сеанса записи/трансляции](https://bugs.kde.org/show_bug.cgi?id=428268) и [показ миниатюр в панели задач](https://bugs.kde.org/show_bug.cgi?id=435588) больше не приводят к аварийному завершению KWin (Alois Wohlschlager, Plasma 5.22)

Эффект «Все окна» [теперь работает в сеансе Wayland при вызове из сгруппированных элементов панели задач](https://bugs.kde.org/show_bug.cgi?id=404953) (David Redondo, Plasma 5.22)

## Исправления и улучшения производительности

Программа просмотра изображений Gwenview [получила обновление, улучшающее скорость, отзывчивость и эффективность расхода памяти](https://invent.kde.org/graphics/gwenview/-/merge_requests/47) при навигации по файловой системе в виде сетки миниатюр, особенно для файлов, которые находятся на сетевых носителях (Arjen Hiemstra, Gwenview 21.08)

При вводе пароля в виджете сетевых подключений [порядок сетей в списке больше не меняется](https://bugs.kde.org/show_bug.cgi?id=435430), что могло приводить к отправке пароля не той сети. Мы довольно долго боролись с этой проблемой разными точечными исправлениями, но теперь, надеюсь, решили её раз и навсегда! (Jan Grulich, Plasma 5.21.5)

Отправка файлов на другие устройства с помощью Bluetooth в Dolphin [снова работает](https://bugs.kde.org/show_bug.cgi?id=409179) (Егор Игнатов, Plasma 5.21.5)

Центр приложений Discover снова исправно [отображает обновления прошивки для поддерживаемых устройств](https://bugs.kde.org/show_bug.cgi?id=435785) (Aleix Pol Gonzalez, Plasma 5.21.5)

[Исправлен один из сценариев аварийного завершения KWin](https://invent.kde.org/plasma/kwin/-/merge_requests/847) при использовании устройства с несколькими видеокартами (Xaver Hugl, Plasma 5.22)

Процессы, которые аварийно завершаются при выходе из системы или входе в неё, [больше не блокируют повторный вход и не вызывают ошибок](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/773) при использовании запуска с помощью Systemd (David Edmundson, Plasma 5.22)

## Улучшения пользовательского интерфейса

[Уточнено начальное представление Discover во время загрузки информации о пакетах](https://bugs.kde.org/show_bug.cgi?id=411397), что понравится пользователям дистрибутивов с медленной реализацией PackageKit (например, openSUSE и производные) (Aleix Pol Gonzalez, Plasma 5.22)

В Системном мониторе Plasma [теперь при открытии любой страницы, на которой есть поисковое поле, оно получает фокус ввода](https://bugs.kde.org/show_bug.cgi?id=435279) (David Redondo, Plasma 5.22)

## Сетевое присутствие

[Обновлён сайт Okular](https://okular.kde.org/ru), благодаря чему он [стал лучше и современнее](https://invent.kde.org/websites/okular-kde-org/-/merge_requests/4) (Carl Schwan)

![2](https://pointieststick.files.wordpress.com/2021/04/screenshot_20210412_090008.png?w=893)

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Богдан Данильченко (expwez)](https://t.me/expwez)  
*Источник:* <https://pointieststick.com/2021/04/16/this-week-in-kde-offline-updates-are-now-optional/>  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icons → значки -->
<!-- 💡: locations → расположения -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: task manager → панель задач -->
<!-- 💡: thumbnail → миниатюра -->
