# На этой неделе в KDE: Resizable Plasma panel pop-ups

> 22–29 мая, основное — прим. переводчика

We are busy working on the bug reports folks are filing about the Plasma 5.25 beta, and as of right now, we’re [down to 15](https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&keywords=regression%2C%20&keywords_type=allwords&list_id=2075033&query_format=advanced&version=5.24.90&version=5.25.0&version=5.25.1&version=5.25.2&version=5.25.3&version=5.25.4&version=5.25.5). Working on these is a great way to make a difference quickly!

In addition, features that didn’t make it into Plasma 5.25 are starting to land in 5.26. There’s some very cool stuff this week, so let’s jump in:

## Исправленные «15-минутные ошибки»

Current number of bugs: **64, up from 63**. 3 added and 2 resolved:

In the Plasma X11 session, Plasma notifications, OSDs, and widget pop-ups [are no longer inappropriately minimizable, maximizable, and tilable](https://bugs.kde.org/show_bug.cgi?id=411462) (Luca Carlon, Plasma 5.26)

[Fixed another way that KWin could crash when plugging in or unplugging an HDMI monitor](https://bugs.kde.org/show_bug.cgi?id=448064) (Xaver Hugl, Plasma 5.24.6)

[Current list of bugs](https://tinyurl.com/plasma-15-minute-bugs)

## Новые возможности

You can now [change the color scheme used by Okular independently of the system color scheme](https://invent.kde.org/graphics/okular/-/merge_requests/461) (George Florea Bănuș, Okular 22.08):

![0](https://pointieststick.files.wordpress.com/2022/05/color-scheme-in-okular.png)

The pop-ups of Plasma widgets in the panel [are now resizable from their edges and corners just like normal windows, and they remember the sizes you set, too](https://bugs.kde.org/show_bug.cgi?id=332512)! (Luca Carlon, Plasma 5.26):

![2](https://pointieststick.files.wordpress.com/2022/05/tall-system-tray.png)

## Исправления ошибок и улучшения производительности

Uninstalling service menu items from Dolphin [now works for services menus that have any symlinks in their sets of installed files](https://bugs.kde.org/show_bug.cgi?id=452289) (Christian Hartmann, Dolphin 22.08)

In the Plasma Wayland session, [Plasma no longer immediately crashes right after login when you have an external screen connected in «switch to external screen» mode](https://bugs.kde.org/show_bug.cgi?id=419492) (someone amazing but probably Влад, Xaver, or Marco; Plasma 5.25)

Multi-line inline messages throughout QtQuick-based software [no longer fail to display their text properly under certain circumstances](https://invent.kde.org/frameworks/kirigami/-/merge_requests/556) (Ismael Asensio, Frameworks 5.95)

When viewing files in the trash, [the process of generating previews for the ones that don’t already have previews no longer results in the files being copied to `/tmp`](https://bugs.kde.org/show_bug.cgi?id=368104) (Méven Car, Frameworks 5.95)

In Konsole, [the «Get new Color Schemes» window once again works properly](https://bugs.kde.org/show_bug.cgi?id=452593) (David Edmundson and Alexander Lohnau, Frameworks 5.95, but distros should be backporting it sooner)

## Улучшения пользовательского интерфейса

The «Eject» button next to mounted disks in Dolphin’s Places panel [no longer appears for internal disks and those manually added to your `/etc/fstab` file](https://bugs.kde.org/show_bug.cgi?id=453890) (Kai Uwe Broulik, Dolphin 22.08)

When you use Spectacle to copy an image to the clipboard, [the notification it sends no longer confusingly talks about saving things](https://bugs.kde.org/show_bug.cgi?id=454269) (Felipe Kinoshita, Spectacle 22.08)

When Okteta (a KDE hex editor app) is installed, [previewing files with Ark no longer opens Okteta unless they are actually binary files](https://invent.kde.org/utilities/ark/-/merge_requests/49) (Nicolas Fella, Ark 22.08)

Discover [now displays a more actionable and relevant error message when you launch it without any app backends available](https://invent.kde.org/plasma/discover/-/merge_requests/307) (Nate Graham, Plasma 5.25):

* ![4](https://pointieststick.files.wordpress.com/2022/05/other-distro-message.png)

You [can now close Discover while software is being installed, removed, or updated, and it will turn into a system progress notification](https://bugs.kde.org/show_bug.cgi?id=419622) (Aleix Pol Gonzalez, Plasma 5.26)

## Как вы можете помочь

Если вы разработчик, посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад. Также взгляните на [ошибки в бета-версии Plasma 5.25](https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&keywords=regression%2C%20&keywords_type=allwords&list_id=2023541&query_format=advanced&version=5.24.90&version=5.25.0&version=5.25.1&version=5.25.2&version=5.25.3&version=5.25.4&version=5.25.5), они тоже важны.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Сергей Гримальский](https://t.me/user32767)  
*Источник:* <https://pointieststick.com/2022/05/27/this-week-in-kde-resizable-plasma-panel-pop-ups>  

<!-- 💡: OSDs → экранные уведомления -->
<!-- 💡: Places panel → панель «Точки входа» -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: get new [thing] → «Загрузить \[что-то\]» -->
<!-- 💡: icon → значок -->
<!-- 💡: system tray → системный лоток -->
