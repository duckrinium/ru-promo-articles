# На этой неделе в KDE: новая веха интерфейса Discover

Мы внесли последние штрихи в Plasma 5.24 и начали работу над выпуском 5.25, сразу же внедрив два больших улучшения: навигацию с клавиатуры для панелей и начальное обновление интерфейса центра приложений Discover! Вот что мы сделали:

> 30 января – 6 февраля, основное — прим. переводчика

## Исправленные «15-минутные ошибки»

Число [ошибок](https://tinyurl.com/plasma-15-minute-bugs) на данный момент: **83, как и на прошлой неделе**.

Plasma, Discover и многие другие приложения [больше не завершают работу аварийно в некоторых ситуациях при запуске с включённой опцией обратной связи](https://bugs.kde.org/show_bug.cgi?id=449505) (Aleix Pol Gonzalez, KUserFeedback 1.1.0)

Изменение свойств пользователя в Параметрах системы [снова работает с версиями системной службы `AccountsService` 22.04.64 и новее](https://bugs.kde.org/show_bug.cgi?id=449385) (Jan Blackquill, Plasma 5.24)

Discover [больше не зависает в случайные моменты при просмотре данных о приложении](https://bugs.kde.org/show_bug.cgi?id=449583) (Aleix Pol Gonzalez, Plasma 5.24.1)

## Wayland

В сеансе Wayland текстовый редактор Kate [больше не мерцает при сохранении пользовательских данных комбинацией Ctrl+S](https://bugs.kde.org/show_bug.cgi?id=435361) (Christoph Cullmann, Kate 22.04)

В сеансе Wayland приложения, использующие прослойку XWayland, больше не перестают реагировать на щелчки мышью [при перетаскивании на их окна объектов](https://bugs.kde.org/show_bug.cgi?id=449362) (David Redondo, Plasma 5.24)

Слой затенения, отображаемый утилитой для создания снимков экрана Spectacle при захвате прямоугольной области, [больше не перекрывается в некоторых случаях другими полноэкранными окнами](https://invent.kde.org/plasma/kwin/-/merge_requests/1957) (Влад Загородний, Plasma 5.24)

## Другие исправления и улучшения производительности

Диспетчер файлов Dolphin [больше не завершает работу аварийно при отмене архивирования, начатого с помощью пунктов контекстного меню «Упаковать»](https://bugs.kde.org/show_bug.cgi?id=446926) (Méven Car, Ark 21.12.3)

При просмотре сервера FTP в Dolphin файлы [снова открываются в указанном пользователем приложении, а не в веб-браузере](https://bugs.kde.org/show_bug.cgi?id=443253) (Nicolas Fella, Dolphin 21.12.3)

При перетаскивании элементов на рабочий стол все они будут расположены в указанном пользователем месте; [ранее туда помещался только один, а остальные располагались в начале сетки после других приложений](https://bugs.kde.org/show_bug.cgi?id=445441) (Severin Von Wnuck, Plasma 5.24)

Discover больше [не завершает работу аварийно, если одновременно удалить или установить несколько приложений в формате Flatpak](https://bugs.kde.org/show_bug.cgi?id=440877) (Aleix Pol Gonzalez, Plasma 5.24)

Discover [стал показывать правильный размер очень больших пакетов](https://invent.kde.org/plasma/discover/-/merge_requests/245) (Jonas Knarbakk, Discover 5.24)

Сеанс Plasma X11 теперь [может использовать настройку глубины цвета в 30 бит](https://bugs.kde.org/show_bug.cgi?id=423014) (Xaver Hugl, Plasma 5.24)

Всплывающее окно системного лотка теперь [имеет фон правильного цвета, когда виджет расположен на рабочем столе](https://bugs.kde.org/show_bug.cgi?id=449535) (Иван Ткаченко, Plasma 5.24.1)

Виджет «Батарея и яркость» [больше не показывает значок низкого заряда, если батареи, о которых известно системе, имеются только во внешних беспроводных устройствах с адекватным уровнем заряда](https://bugs.kde.org/show_bug.cgi?id=448797) (Aleix Pol Gonzalez, Plasma 5.25)

Система ввода-вывода KIO больше не делает [безуспешных попыток открыть URL-адреса, которые принадлежат приложениям]((https://invent.kde.org/frameworks/kio/-/merge_requests/719)) (например, `tg://` для Telegram или `mailto://` для почтового клиента) (Nicolas Fella, Frameworks 5.91)

Перезапуск диспетчера окон KWin [больше не приводит к прекращению работы всех его комбинаций клавиш](https://bugs.kde.org/show_bug.cgi?id=448369) (например, Alt+Tab) (Влад Загородний, Frameworks 5.91)

Логотип KDE Plasma из набора значков Breeze стал [лучше виден на всплывающих уведомлениях при использовании тёмной цветовой схемы](https://bugs.kde.org/show_bug.cgi?id=416302) (Gabriel Knarlsson, Frameworks 5.91)

Исправлено [несколько несоответствий в значках папок и некоторых типов файлов в разных размерах](https://invent.kde.org/frameworks/breeze-icons/-/merge_requests/198) (Gabriel Knarlsson, Frameworks 5.91)

## Улучшения пользовательского интерфейса

Теперь можно [перемещать вкладки между окнами и створками Kate](https://bugs.kde.org/show_bug.cgi?id=426768) (Waqar Ahmed, Kate 22.04)

Боковая панель закладок Okular [получила более понятный интерфейс](https://invent.kde.org/graphics/okular/-/merge_requests/488) (Nate Graham, Okular 22.04):

![0](https://pointieststick.files.wordpress.com/2022/02/screenshot_20210924_151258.png)

При сжатии нескольких файлов с помощью контекстного меню [Dolphin стал отображать имя конечного архива](https://bugs.kde.org/show_bug.cgi?id=446728) (Fushan Wen, Ark 22.04)

Эмулятор терминала Konsole [теперь можно найти среди установленных приложений по запросам «cmd» и «командная строка»](https://invent.kde.org/utilities/konsole/-/merge_requests/592) («M B», Konsole 22.04)

При поиске страниц Параметров системы [предпочтение теперь отдаётся полным совпадениям ключевых слов](https://invent.kde.org/plasma/systemsettings/-/merge_requests/123) (Alexander Lohnau, Plasma 5.24)

Теперь [Discover нельзя использовать для удаления самого себя](https://bugs.kde.org/show_bug.cgi?id=449260) (Nate Graham, Plasma 5.24)

Страницы приложений в Discover [были переработаны для большей эстетики и удобства использования](https://invent.kde.org/plasma/discover/-/merge_requests/246) (Nate Graham и Manuel Jésus de la Fuente, Plasma 5.25):

* ![1](https://pointieststick.files.wordpress.com/2022/02/desktop_-_krita.png)
* ![2](https://pointieststick.files.wordpress.com/2022/02/desktop_-_endless_sky.png)

Стало возможным использовать сочетание клавиш (по умолчанию Meta+Alt+P) [для переключения фокуса между панелями и активации виджетов с помощью клавиатуры](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/641) (Marco Martin, Plasma 5.25)

Окно настроек диспетчера буфера обмена [стало интуитивно понятнее](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1065) (Jonathan Marten, Plasma 5.25)

## Как вы можете помочь

Если вы разработчик, посмотрите нашу новую инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Виталий Рябичев](https://t.me/bunker203)  
*Источник:* <https://pointieststick.com/2022/02/04/this-week-in-kde-discover-redesign-has-begun/>  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: location → расположение -->
<!-- 💡: system tray → системный лоток -->
